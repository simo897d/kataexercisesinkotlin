package com.example.katas.nbYear

fun nbYear(pp0:Int, percent:Double, aug:Int, p:Int):Int{
    var sum: Double = pp0.toDouble()
    var year: Int = 0
    while(sum < p){
        sum += (sum * (percent/100)) + aug
        year++
    }
    return year
}
