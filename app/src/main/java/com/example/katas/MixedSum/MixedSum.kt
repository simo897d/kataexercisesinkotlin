package com.example.katas.MixedSum

public class MixedSum {

    /*
    * Assume input will be only of Int or String type
    */
    public fun sum(mixed: List<Any>): Int {
        var sum: Int = 0
        for(c in mixed){
            var a = c.toString()
            var b = a.toInt()
            sum += b
        }
        return sum
    }
}