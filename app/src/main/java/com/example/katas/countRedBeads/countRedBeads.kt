package com.example.katas.countRedBeads

fun countRedBeads(nBlue: Int): Int {
    return when (nBlue) {
        0, 1 -> return 0
        else -> nBlue * 2 - 2
    }
}
/*
Alternatively
fun countRedBeads(nBlue: Int): Int = if (nBlue < 2) 0 else (nBlue - 1) * 2
 */